import discord
from discord.ext import commands
import datetime
import asyncio
import random

client = commands.bot(command_prefix = "/", help = "help")

f = open("Token.env", 'r')
TOKEN = f.read()
@client.command()
@commands.has_permission(admin=True)
async def giveawaystart(ctx, mins: int, prize: str):
    embed = discord.Embed(title= "Giveaway!", description= f"{prize}", color=ctx.author.color)
    end = datetime.datetime.now() + datetime.timedelta(seconds= mins*60)
    embed.add_field(name= "Ends at:", value=f"{end}")
    embed.set_footer(text= f"Ends {mins} minutes from now!")
    my_msg = await ctx.send(embed = embed)
    await my_msg.add_reaction('🎉')
    await asyncio.sleep(mins*60)
    new_msg = await ctx.channel.fetch_channel(my_msg.id)
    users = new_msg.reactions[0].users().flatten()
    users.pop(users.index(client.user))
    winner = random.choice(users)
    await ctx.send(f"Congratulations, {winner.mention} won {prize}!")

client.run(TOKEN)